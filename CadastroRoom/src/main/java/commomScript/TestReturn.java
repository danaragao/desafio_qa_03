package commomScript;

public class TestReturn {
	
	String AcessoValido;
	String AcessoNegado;
	String CadastroAprovado;
	String CadastroNegado;

	public String getCadastroAprovado() {
		return CadastroAprovado;
	}

	public void setCadastroAprovado(String cadastroAprovado) {
		CadastroAprovado = cadastroAprovado;
	}

	public String getCadastroNegado() {
		return CadastroNegado;
	}

	public void setCadastroNegado(String cadastroNegado) {
		CadastroNegado = cadastroNegado;
	}

	public String getAcessoNegado() {
		return AcessoNegado;
	}

	public void setAcessoNegado(String acessoNegado) {
		AcessoNegado = acessoNegado;
	}

	public String getAcessoValido() {
		return AcessoValido;
	}

	public void setAcessoValido(String acessoValido) {
		AcessoValido = acessoValido;
	}

}
