package commomScript;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CadastroNovoRoomGeneral {

	static WebDriver driver;
	Util util;

	public CadastroNovoRoomGeneral(WebDriver driver) {
		this.driver = driver;
		this.util = new Util(driver);

	}

	public TestReturn cadastroRoomGeneral(String preco, String qtd,
			String qtdEstadia, String qtdAdulto, String qtdCrianca,
			String extraBeds, String bedCharges) {

		TestReturn retorno = new TestReturn();

		WebElement menuHotels = util.menuHotels(1000, 20);
		menuHotels.click();

		WebElement menuAddRoom = util.menuAddRoom(100, 3);
		menuAddRoom.click();

		Select campoStatus = new Select(driver.findElement(By
				.xpath("//select//option[contains(text(), \"Enabled\")]")));

		campoStatus.selectByVisibleText("Enabled");

		WebElement campoRoomType = util.campoRoomT(100, 2);
		campoRoomType.click();

		WebElement resultadoRoomT = util.resultadoRoomT(100, 3);
		resultadoRoomT.click();

		WebElement campoPric = util.campoPrice(100, 3);
		campoPric.sendKeys(preco);

		WebElement campoQuantity = util.campoQtd(100, 3);
		campoQuantity.sendKeys(qtd);

		WebElement campoMinimumSt = util.campoMinimoSt(100, 3);
		campoQuantity.sendKeys(qtdEstadia);

		WebElement campoMaxAdults = util.campoMaxAd(100, 3);
		campoMaxAdults.sendKeys(qtdAdulto);

		WebElement campoMaxChildren = util.campoMaxCh(100, 3);
		campoMaxChildren.sendKeys(qtdCrianca);

		WebElement campoExtraBeds = util.campoNoExtraBeds(100, 3);
		campoExtraBeds.sendKeys(extraBeds);

		WebElement campoExtraBedCharges = util.campoExtraBedCh(100, 3);
		campoExtraBedCharges.sendKeys(bedCharges);

		WebElement botaoSubmit = util.botaoSub(100, 3);
		botaoSubmit.click();

		try {
			if ((botaoSubmit != null)) {

				retorno.setCadastroNegado("Cadastro room general negado!");
			}

		} catch (NoSuchElementException e) {

			retorno.setCadastroAprovado("Cadastro concluido com sucesso!");

		}
		return retorno;
	}
}
