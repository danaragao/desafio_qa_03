package commomScript;

import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginCadastroRoom {

	static WebDriver driver;
	Util util;

	public LoginCadastroRoom(WebDriver driver) {
		this.driver = driver;
		this.util = new Util(driver);

	}

	public TestReturn login(String email, String senha) {

		TestReturn retorno = new TestReturn();

		WebElement campoEmail = util.campoEmail(50, 2);
		campoEmail.sendKeys(email);

		WebElement campoSenha = util.campoSenha(100, 2);
		campoSenha.sendKeys(senha);

		WebElement botaoEntrar = util.botaoEntrar(100, 2);
		botaoEntrar.click();

		try {

			if ((botaoEntrar != null)) {

				retorno.setAcessoValido("Login v�lido!");

			}

		} catch (NoSuchElementException e) {

			retorno.setAcessoNegado("Credenciais de login inv�lidas!");

		}
		return retorno;
	}
}
