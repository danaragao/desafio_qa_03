package commomScript;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Util {
	
	WebDriver driver;

	public Util(WebDriver driver) {
		this.driver = driver;
	}
	
	
//	--------------------Login--------------------------------
	
	public WebElement campoEmail(int intervalo, int timeout) {

		return this.buscarElemento("//input[@name=\"email\"]", intervalo, timeout);

	}
	
	public WebElement campoSenha(int intervalo, int timeout) {

		return this.buscarElemento("//input[@name=\"password\"]", intervalo, timeout);

	}
	
	public WebElement botaoEntrar(int intervalo, int timeout) {

		return this.buscarElemento("//button[@type=\"submit\"]", intervalo, timeout);

	}
	
	public WebElement credencialinvalida(int intervalo, int timeout) {

		return this.buscarElemento("//div//font[contains(text(), \"credenciais de login inválidas\")]", intervalo, timeout);

	}
	
	
//	--------------------CadastroGeneral--------------------------------
	
	public WebElement menuHotels(int intervalo, int timeout) {

		return this.buscarElemento("//li//a[@href=\"#Hotels\"]", intervalo, timeout);

	}
	
	public WebElement menuAddRoom(int intervalo, int timeout) {

		return this.buscarElemento("//a[contains(text(), \"Add Room\")]", intervalo, timeout);

	}
	
	public WebElement campoRoomT(int intervalo, int timeout) {

		return this.buscarElemento("//a//span[contains(text(), \"Room Type\")]", intervalo, timeout);

	}
	
	public WebElement resultadoRoomT(int intervalo, int timeout) {

		return this.buscarElemento("//li//div[contains(text(), \"Two-Bedroom Apartment\")]", intervalo, timeout);

	}
	
	public WebElement campoPrice(int intervalo, int timeout) {

		return this.buscarElemento("//div//input[@placeholder=\"Price\"]", intervalo, timeout);

	}
	
	public WebElement campoQtd(int intervalo, int timeout) {

		return this.buscarElemento("//div//input[@placeholder=\"Quantity\"]", intervalo, timeout);

	}
	
	public WebElement campoMinimoSt(int intervalo, int timeout) {

		return this.buscarElemento("//div//input[@placeholder=\"Minimum Stay\"]", intervalo, timeout);

	}
	
	public WebElement campoMaxAd(int intervalo, int timeout) {

		return this.buscarElemento("//div//input[@placeholder=\"Max Adults\"]", intervalo, timeout);

	}
	
	public WebElement campoMaxCh(int intervalo, int timeout) {

		return this.buscarElemento("//div//input[@placeholder=\"Max Children\"]", intervalo, timeout);

	}
	
	public WebElement campoNoExtraBeds(int intervalo, int timeout) {

		return this.buscarElemento("//div//input[@placeholder=\"Extra beds\"]", intervalo, timeout);

	}
	
	public WebElement campoExtraBedCh(int intervalo, int timeout) {

		return this.buscarElemento("//div//input[@placeholder=\"Beds charges\"]", intervalo, timeout);

	}
	
	public WebElement botaoSub(int intervalo, int timeout) {

		return this.buscarElemento("//button[@id=\"add\"]", intervalo, timeout);

	}
	
	public WebElement buscarElemento(String xpath, int intervalo, int timeout) {
		for (int i = 0; i < timeout; i++) {
			try {
				Thread.sleep(intervalo);
			} catch (InterruptedException e) {
			}
			WebElement element = driver.findElement(By.xpath(xpath));
			if (element == null) {
				continue;
			}

			return element;
		}
		return null;
	}

}
