package testProjetoInmetrics;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import commomScript.CadastroNovoRoomGeneral;
import commomScript.LoginCadastroRoom;
import commomScript.TestReturn;

public class CadastroRoomGeneral {
	
	static WebDriver driver;
	static LoginCadastroRoom login;
	static CadastroNovoRoomGeneral cadastro;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:/Users/Witor/Downloads/chromedriver.exe");
		driver = new ChromeDriver();
		// Acessando a url
		driver.get("https://www.phptravels.net/supplier");
		driver.manage().window().maximize();
		login = new LoginCadastroRoom(driver);
	}



	@Test
	public void test() {
		
		TestReturn testLogin = login.login("supplier@phptravels.com", "demosupplier");
		
		TestReturn testCadastroRoom = cadastro.cadastroRoomGeneral("300", "2", "30", "4", "2", "1", "2");
		
		System.out.println("Acesso Valido: "+testLogin.getAcessoValido());
		System.out.println("Acesso Invalido: "+testLogin.getAcessoNegado());
		System.out.println("Cadastro Efetuado: "+testCadastroRoom.getCadastroAprovado());
		System.out.println("Cadastro Negado: "+testCadastroRoom.getCadastroNegado());
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		driver.close();
	}

}
